#!/usr/bin/env bash

BASEDIR=$(dirname "${BASH_SOURCE[0]}")
cd ${BASEDIR}
. create_env.sh

docker-compose -f ${DEV_DOCKER_COMPOSE_FILE} down
