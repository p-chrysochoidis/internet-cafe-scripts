#!/usr/bin/env bash

BASEDIR=$(dirname "${BASH_SOURCE[0]}")
cd ${BASEDIR} && . create_env.sh

CONTAINER_ID=$(docker-compose -f ${DEV_DOCKER_COMPOSE_FILE} ps -q ${CLIENT_SERVICE_NAME})

gnome-terminal -- docker exec -it ${CONTAINER_ID} bash