#!/usr/bin/env bash

BASEDIR=$(dirname "${BASH_SOURCE[0]}")
cd ${BASEDIR} && . create_env.sh

SERVER_CONTAINER_ID=$(docker-compose -f ${DEV_DOCKER_COMPOSE_FILE} ps -q ${SERVER_SERVICE_NAME})

echo
echo "Server IP: " $(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ${SERVER_CONTAINER_ID})
echo