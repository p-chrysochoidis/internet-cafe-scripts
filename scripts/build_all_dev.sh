#!/usr/bin/env bash

BASEDIR=$(dirname "${BASH_SOURCE[0]}")
cd ${BASEDIR}
. create_env.sh

cd "${CLIENT_DEV_BUILD_SCRIPT_FOLDER}" && . build-dev-docker.sh || exit 1
cd "${SERVER_DEV_BUILD_SCRIPT_FOLDER}" && . build-dev-docker.sh || exit 1
