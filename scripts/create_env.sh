#!/usr/bin/env bash

BASEDIR=$(dirname "${BASH_SOURCE[0]}")

export SERVER_SERVICE_NAME="internet-cafe-server"
export CLIENT_SERVICE_NAME="internet-cafe-client-pc"
export CLIENT_DEV_BUILD_SCRIPT_FOLDER="$(cd ${BASEDIR} && cd ../../internet-cafe-client-pc-app/scripts/ && pwd)"
export SERVER_DEV_BUILD_SCRIPT_FOLDER="$(cd ${BASEDIR} && cd ../../internet-cafe-server/scripts/ && pwd)"
export DOCKER_COMPOSE_FOLDER="$(cd ${BASEDIR} && cd ../docker-compose && pwd)"
export DEV_DOCKER_COMPOSE_FILE="$(cd ${DOCKER_COMPOSE_FOLDER} && pwd)/dev.yalm"

# used in docker-compose files
export CLIENT_APP_ROOT="$(cd ${BASEDIR} && cd ../../internet-cafe-client-pc-app && pwd)"
export SERVER_APP_ROOT="$(cd ${BASEDIR} && cd ../../internet-cafe-server && pwd)"
export DOCKER_USER=$(id -u)
