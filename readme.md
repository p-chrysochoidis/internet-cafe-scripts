### This project contains scripts (bash/docker-compose) to make development\running of `internet-cafe` app easier

_The scripts assumes that the `internet-cafe-server` and `internet-cafe-client-app`
projects are located at the same directory as the root folder of this project._

##### Available scripts

* `build_all_dev.sh` Builds the dev images for both projects (client and server) that
are required by the `./docker-compose/dev.yalm`

* `start_dev.sh` Starts the server and the client dev images with the app folders attached.
Also launches one `gnome-terminal` for each one.

* `stop_dev.sh` Stops all the dev services.